angular.module('starter.services', ['angularPaho'])

  .factory('User', function ($http) {

    var loggedIn = false;

    return {
      login: function (credentials) {
        let data = {
          'username': credentials.username,
          'password': credentials.password
        };

        let config = {
          'Content-Type': 'application/json'
        };
        loggedIn = true;
        return $http.post('http://192.168.0.38:1880/login', data, config);
      },

      isLoggedIn: function () {
        return loggedIn;
      }

    };
  })

  .factory('DHT22', function ($http) {
    return {
      getData: function () {
        return $http.get('http://192.168.0.5:1880/dht11/data');
        //return $http.get('https://randomuser.me/api/?results=1');
      }
    };
  })

  .factory('Led', function ($http) {
    let data = {
      'led': false
    };
    let config = {
      'Content-Type': 'application/json'
    };
    return {
      ledOff: function () {
        return $http.post('http://192.168.0.38:1880/led/1', data, config);
      },

      ledOn: function () {
        let data = {
          'led': true
        };
        return $http.post('http://192.168.0.38:1880/led/1', data, config);
      }
    };
  })

  // Singleton
  .factory('ClienteSingleton', function (MqttClient) {
    var ip = '192.168.0.5';
    var port = '9001';
    var id = "";
    MqttClient.init(ip, port, id);
    var message = MqttClient.message;
    return {
      getMessage: function () {
        message = MqttClient.message;
        return message;
      }
    };
  })
  ;
