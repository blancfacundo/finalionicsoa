// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'angularPaho'])

  .run(function ($ionicPlatform, $rootScope, $state, User, MqttClient) {



    // let client = new Paho.MQTT.Client("192.168.0.5", Number(9001), "/ws", "clientId");

    // // set callback handlers
    // client.onConnectionLost = function (responseObject) {
    //     console.log("Connection Lost: "+responseObject.errorMessage);
    // }

    // client.onMessageArrived = function (message) {
    //   console.log("Message Arrived: "+message.payloadString);
    // }

    // // Called when the connection is made
    // function onConnect(){
    //   console.log("Connected!");
    // }

    // // Connect the client, providing an onConnect callback
    // client.connect({
    //   onSuccess: onConnect, 
    //   userName : 'soa2019',
    //   password : 'soa2019'
    // });

    $rootScope.message = MqttClient.message;

    $rootScope.$on('$stateChangeStart', function (event, toState) {

      //var ip = '192.168.0.5';
      //var port = '9001';
      //var id = '';


      //MqttClient.init(ip, port, id);
      // MqttClient.connect({onSuccess: successCallback, username: 'soa2019', password: 'soa2019'});
      //MqttClient.connect({onSuccess: successCallback});
      //function successCallback() {
      //console.log("me conecte");
      //MqttClient.subscribe('/World');
      //message = new Paho.MQTT.Message("Hello");
      //message.destinationName = "/World";
      //MqttClient.send(message);
      //}

      if (!User.isLoggedIn() && toState.name !== 'login') {         //Si no estamos logeados vamos al estado login.
        event.preventDefault();                                     //El estado login nos controla todo acerca del acceso
        $state.go('login');
      }
    });

    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs).
      // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
      // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
      // useful especially with forms, though we would prefer giving the user a little more room
      // to interact with the app.
      // if (window.cordova && window.Keyboard) {
      //   window.Keyboard.hideKeyboardAccessoryBar(true);
      // }
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // Set the statusbar to use the default style, tweak this to
        // remove the status bar on iOS or change it to use white instead of dark colors.
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {

    //MQTTProvider.hola('que tal');

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider


      .state('login', {
        url: '/',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      })

      // setup an abstract state for the tabs directive
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html',
        controller: 'TabCtrl'
      })

      // Each tab has its own nav history stack:

      .state('tab.led', {
        url: '/led',
        views: {
          'tab-led': {
            templateUrl: 'templates/tab-led.html',
            controller: 'LedCtrl'
          }
        }
      })

      .state('tab.actuador', {
        url: '/actuador',
        views: {
          'tab-actuador': {
            templateUrl: 'templates/tab-data.html',
            controller: 'ActuadoresCtrl'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/led');

  });
