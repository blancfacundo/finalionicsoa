angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Chats) {

  Chats.randomme(4).then(
    function(r){
      //console.log(r.data)
      $scope.lista = r.data.results
    },
  function(){}
  );
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})


.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('RaspiCtrl', function($scope, DHT22) {
  DHT22.getData().then(
    function(r){
      $scope.datosDhT = r.data
      console.log(r.data)
    },
  function(){}
  );
});
